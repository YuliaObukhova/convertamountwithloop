import java.util.Scanner;

//Напишите программу, которая конвертирует сумму денег из юаней в российские рубли,
//добавив структуру выбора для принятия решений об окончаниях входной валюты в зависимости от ее значения,
//цикл проверки правильности ввода количества конвертаций и счетный цикл для многократных конвертаций сумм,
//метод, отображающий инструкции, и метод, конвертирующий сумму денег,
//одномерные массивы сумм денег для многократных конвертаций.


public class ConvertAmountWithLoop {
    static final double ROUBLES_PER_YUAN = 12.58;

    public static void main(String[] args) {
        int[] yuanArray;
        double[] roublesArray;
        int n;
        int i;

        Scanner input = new Scanner(System.in);

        //отобразить инструкцию
        instruct();

        //получать количество конвертаций до тех пор,
        // пока не введено корректное значение
        do {
            System.out.print("Введите корректное количество конвертаий: ");
            n = input.nextInt();
        } while (n <= 0);


        //получить n сумм денег в юанях
        System.out.println("Введите " + n + " сумм денег в юанях через пробел: ");
        yuanArray = new int[n];
        for (i = 0; i < n; ++i) {
            yuanArray[i] = input.nextInt();
        }

        //конвертировать n сумм денег в российские рубли
        roublesArray = findRoubles(yuanArray, n);

        // отобразить в таблице n сумму денег в юанях
        // и эквивалентные им суммы денег в российских рублях
        // в пользу покупателя
        System.out.println("\n  Cумма, ₽    Сумма, ¥");
        for (i = 0; i < n; ++i) {
            System.out.println("\t" + yuanArray[i] + "\t" + "\t" + "\t" + (int) (roublesArray[i] * 100) / 100.0);
        }
    }

    /**
     * отображает инструкцию
     */
    public static void instruct() {
        System.out.println("Эта программа конвертирует сумму денег " + "из юаней в российские рубли.");
        System.out.println("Курс покупки равен " + ROUBLES_PER_YUAN + " рубля. \n");
    }

    /**
     * конвертирует n сумм денег из юаней в рубли
     */
    public static double[] findRoubles(int[] yuanArray, int n) {
        double[] roublesArray = new double[n];
        int i;
        for (i = 0; i < n; ++i)
            roublesArray[i] = ROUBLES_PER_YUAN * yuanArray[i];
        return roublesArray;
    }
}
